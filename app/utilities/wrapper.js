'use strict';

let wrapResponse = async (results, statusCode) => { 
    return {
        headers: { 
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(results),
        statusCode: statusCode || 200
    }
}

let wrapError = async (err) => {
    // Default case.
    return {
        headers: { 
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            msg: err.message || err.msg || 'Internal server error',
            statusCode: err.statusCode || err.status || 500
        }),
        statusCode: err.statusCode || err.status || 500
    }
}

module.exports = {
    wrapResponse,
    wrapError
}