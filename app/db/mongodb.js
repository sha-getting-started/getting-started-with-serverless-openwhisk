'use-strict';

const bluebird = require('bluebird');
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_HOST, { 
    useNewUrlParser: true
});

// Options for deprecated methods.
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

// Promisify all mongoose functions.
// No need to use the prefix Async.
mongoose.Promise = bluebird;

let db = mongoose.connection;

db.on('connected', () => {
    console.log('MongoDb connected.');
});

db.on('error', (err) => {
    console.log('Unable to connect MongoDb.', err);
});