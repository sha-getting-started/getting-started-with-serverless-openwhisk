'use strict';

const jwt = require('jsonwebtoken');
const Users = require('../models/user');

/*
* Create new user.
*/
let signup = async (user) => {
    user = new Users(user);
    user = await user.save();
    user = user.toJSON();
    delete user.password;
    return user;
}

/*
* Sign in user.
* Rejects if user is not verified by both methods.
*/
let signin = async (credentials) => {
    let user = await Users.findOne({ _id: credentials._id });

    // Return error if user not found.
    if(!user) {
        return Promise.reject({
            message: 'User not found.',
            statusCode: 404
        });
    }

    // Return error if credentials don't match.
    if(!user.checkPassword(credentials.password)) {
        return Promise.reject({
            message: 'Invalid password.',
            statusCode: 401
        });
    }

    // Create token.
    let token = jwt.sign({ _id: user._id }, process.env.SECRET_KEY, 
        { expiresIn: parseInt(process.env.TOKEN_EXPIRY) });

    return {
        token,
        expiresIn: parseInt(process.env.TOKEN_EXPIRY)
    };
}

/*
* Get all users.
* TODO: Implement querying.
*/
let get = async () => {
    let users =  await Users.find({ }, { password: 0 });
    return users;
}

/*
* Get user by id.
*/
let getById = async (userId) => {
    let user = await Users.findOne({ _id: userId }, { password: 0 });

    // Return error if user not found.
    if(!user) {
        return Promise.reject({
            message: 'User not found.',
            statusCode: 404
        });
    }

    return user;
}

/*
* Update user by id.
* If password is changed, it is then handled by mongoose schema.
*/
let update = async (userId, properties) => {
    let user = await Users.findOne({ _id: userId });

    // Return error if user not found.
    if(!user) {
        return Promise.reject({
            message: 'User not found.',
            statusCode: 404
        });
    }

    // Update all properties.
    for(let property in properties) {
        user[property] = properties[property];
    }

    user = await user.save();
    user = user.toJSON();
    delete user.password;
    return user;
}

/*
* Delete user by id.
*/
let remove = async (userId) => {
    let user = await Users.findOneAndRemove({ _id: userId });

    // Return error if user not found.
    if(!user) {
        return Promise.reject({
            message: 'User not found.',
            statusCode: 404
        });
    }

    user = user.toJSON();
    delete user.password;
    return user;
}

module.exports = {
    signup,
    signin,
    get,
    getById,
    update,
    remove
}