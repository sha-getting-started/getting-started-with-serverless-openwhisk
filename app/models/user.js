'use strict';

const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    _id: { 
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: false
    },
    lastname: {
        type: String,
        required: false
    },
    company: {
        type: String,
        required: false
    },
    role: {
        type: String,
        required: true
    },

    phoneNumber: {
        type: String,
        required: false
    },
    phoneNumberVerified: {
        type: Boolean,
        required: false
    },

    email: {
        type: String,
        required: false
    },
    emailVerified: {
        type: Boolean,
        required: false
    },

    city: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: false
    },

    created: {
        type: Date, 
        default: moment.utc()
            .add(process.env.TIME_DIFFERENCE, 'hours')
    },
    image: {
        type: String,
        required: false
    },
    tags: [{
        type: String,
    }]
},
{  
    _id: false,
    id: false,
    versionKey: false,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true 
    }
});

/*
* Pre function executed before save method.
* Hash the password before saving.
* Only hash password if modified.
* Don't use arrow function as it would change 'this' scope.
*/
UserSchema.pre('save', function(next) {
    let user = this;

    // Make sure password is not double hashed.
    if(!user.isModified('password')) {
        return next();
    }

    try {
        // Encrypt password.
        let salt = bcrypt.genSaltSync(10);
        let hash = bcrypt.hashSync(user.password, salt);

        user.password = hash;
        return next();
    } catch (err) {
        return next(err);
    }
});

/* 
* Instance method to check password.
* Don't use arrow function as it would change 'this' scope.
*/
UserSchema.methods.checkPassword = function (password) {
    try {
        return bcrypt.compareSync(password, this.password);
    } catch(err) {
        throw err;
    }
}

module.exports = mongoose.model('User', UserSchema);