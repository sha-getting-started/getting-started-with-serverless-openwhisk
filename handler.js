'use strict';

require('dotenv').config({ path: './config/dev.env' });
require('./app/db/mongodb');
const usersRepo = require('./app/repos/users');
const wrapper = require('./app/utilities/wrapper');

let signup = async (params) => {
   try {
       let results = await usersRepo.signup(params.user);
       return wrapper.wrapResponse(results, 201);
   } catch(err) {
       return wrapper.wrapError(err);
   }
}

let signin = async (params) => {
    try {
        let results = await usersRepo.signin(params.user);
        return wrapper.wrapResponse(results);
    } catch(err) {
        return wrapper.wrapError(err);
    }
}

let get = async (params) => {
    try {
        let results = await usersRepo.get();
       return wrapper.wrapResponse(results);
    } catch(err) {
        return wrapper.wrapError(err);
    }
}

let getById = async (params) => {
    try {
        // Parse param from route;
        let userId = params.__ow_path.split('/')[2];
        let results = await usersRepo.getById(userId);
       return wrapper.wrapResponse(results);
    } catch(err) {
        return wrapper.wrapError(err);
    }
}

let update = async (params) => {
    try {
        // Parse param from route;
        let userId = params.__ow_path.split('/')[2];
        let results = await usersRepo.update(userId, params.user);
       return wrapper.wrapResponse(results);
    } catch(err) {
        return wrapper.wrapError(err);
    }
}

let remove = async (params) => {
    try {
        // Parse param from route;
        let userId = params.__ow_path.split('/')[2];
        let results = await usersRepo.remove(userId);
       return wrapper.wrapResponse(results);
    } catch(err) {
        return wrapper.wrapError(err);
    }
}

module.exports = {
    signup,
    signin,
    get,
    getById,
    update,
    remove
}