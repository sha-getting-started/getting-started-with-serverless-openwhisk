'use strict';

// Load testing environment.
require('dotenv').config({ path: './config/dev.env' });

const chai = require('chai');
const expect = chai.expect;

require('../app/db/mongodb');
const usersRepo = require('../app/repos/users');

describe('Users repository', () => {
    let user = require('../test/users-data.json');
    let newUser = user.newUser;
    let updatedUser = user.updatedUser;
    let token = '';

    before(async () => {
        setTimeout(() => {
           // Wait for some time before database connect.
        });
    }, 1500);

    
    it('Create new user', async () => {
        let user = await usersRepo.signup({ ...newUser });
        expect(user._id).to.equal(newUser._id);
    });

    it('Get users', async () => {
        let users = await usersRepo.get();
        expect(users[0]._id).to.equal(newUser._id);
    });


    it('Get user by id', async () => {
        let user = await usersRepo.getById(newUser._id);
        expect(user._id).to.equal(newUser._id);
    });


    it('Signin user', async () => {
        let user = await usersRepo.signin({ 
            _id: newUser._id,
            password: newUser.password
        });

        expect(user).to.have.property('token');
        expect(user).to.have.property('expiresIn');
    });

    it('Update user', async () => {
        let user = await usersRepo.update(newUser._id, { ...updatedUser });

        for(let property in updatedUser) {
            expect(newUser[property]).to.not.equal(user[property]);
        }
    });

    it('Signin user after password change', async () => {
        let user = await usersRepo.signin({ 
            _id: newUser._id,
            password: updatedUser.password
        });

        expect(user).to.have.property('token');
        expect(user).to.have.property('expiresIn');
    });


    it('Remove user', async () => {
        let user = await usersRepo.remove(newUser._id);
        expect(user._id).to.equal(newUser._id);
    });
});